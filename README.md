# Kobuki Robot Housing
![Picture of the Kobuki robot chassis, which was used in the past for the Turtlebot 2. Currently 
the Turtlebot2 is no longer in use, but the Kobuki chassis are still 
supported by ros2. I got the picture from: https://www.tegakari.net/wp-content/uploads/2016/10/kobuki_img-640x381.jpg; Called on 12/09/2023 at 11:52 AM](./pictures/kobuki_chassis.jpg)  
One of [Mamut](https://www.thm.de/ei/studium/praxis/ags-workshops/m-a-m-u-t.html) main goals is to make robots accessible to everyone. To achieve this 
goal, we're working on several robotics projects on different robotics platforms. A few years ago we borrowed 2 
[Turtlebot2](https://www.turtlebot.com/turtlebot2/) robots, which are currently gathering dust in our locker, never used. I found out that the 
[ROS2](https://docs.ros.org/en/humble/index.html) [Kobuki driver](https://github.com/kobuki-base), the little round robot you see above, is still 
being maintained, so I got the idea that it would be nice to put a Single-board computer, a central microcontroller and some sensors on it and it 
would make a great robot for teaching purposes.
## getting started
### Compiling the main mesh_file source code
If you intend to use this file for 3D printing, compile the main mesh file's source code, which is the 'mount.scad'. You need to have "OpenSCAD," 
a code-based CAD program, to achieve this. In addition, OpenSCAD is a compiler for .scad files. If you have successfully installed the compiler, 
you have various options to compile the .stl file for your slicer.
#### Graphical Way
If you prefer not to use the terminal or are using Windows, you may use the graphical user interface of OpenSCAD. Begin by launching the program, 
then navigate to the left-hand side and select the "File" button. Next, click "Open file" to load the `mount.scad` from the repository and wait 
for it to fully load.  After loading the file, you can create an .stl file by selecting `File`, then `Export`, and finally `Export as STL...`.
#### Programatical Way
Under Linux, simply open the terminal in the repository directory and enter:  
```
openscad -o kobuki_mount.stl mount.scad
```
to compile the main mesh source code into a .stl for your slicer. I would recommend to extend this command, because compiling the output into 
the main project folder is bad practice. For example:
```
openscad -o ~/3DPrints/kobuki_mount.stl mount.scad
```
This command will simply compile the source code into a .stl and put it into a folder.
### Hardware needed:
- kobuki robot
- raspberry pi 4 or later
- stm32 nucleo stile microcontroller board.
